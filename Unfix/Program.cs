using DocoptNet;
using F23.StringSimilarity;
using F23.StringSimilarity.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Unfix {
    public class Program
    {
        private const string Version = "Unfix 1.0";
        private const string Usage =
@"Usage:
    Unfix <word> using <corpus> [-r RANGE] [-l LIMIT] [-f FREQUENCY_WEIGHT] [-s SIMILARITY_WEIGHT]
    Unfix (-h | --help)
    Unfix --version

Options:
    -h --help               Show this text.
    --version               Show version.
    <word>                  The word to unfix
    <corpus>                The word list to use when unfixing
    -r RANGE                The allowable size differential between the original word and suggested unfixes [default: 2].
    -l LIMIT                The number of results to return [default: 10].
    -f FREQUENCY_WEIGHT     The weight of the frequency-based measure [default: 1.0]
    -s SIMILARITY_WEIGHT    The weight of teh similiar-based measure [default: 1.0]

";
        private static (int, Dictionary<string, int>) ParseWordLsit(string corpus,
            int minWordLength, int maxWordLength)
        {
            var wordFrequencies = new Dictionary<string, int>();
            Console.WriteLine($"Reading corpus: {corpus}");

            Regex lowercaseRegex = new Regex("^[a-z]+$");
            int maxFrequency = 0;

            foreach (string line in File.ReadAllLines(corpus)) {
                string[] record = line.Split(' ');
                string word = record[0];
                if (word.Length < minWordLength || word.Length > maxWordLength) {
                    continue;
                }
                if (!lowercaseRegex.IsMatch(word)) {
                    continue;
                }
                if (!int.TryParse(record[1], out int frequency)) {
                    continue;
                }
                maxFrequency = Math.Max(maxFrequency, frequency);
                wordFrequencies[word] = frequency;
            }
            return (maxFrequency, wordFrequencies);
        }

        private static void LogTask(ref DateTimeOffset startTime, string taskDescription)
        {
            DateTimeOffset endTime = DateTimeOffset.Now;
            TimeSpan span = endTime - startTime;
            Console.WriteLine($"{span.TotalMilliseconds:,0} ms spent {taskDescription}.");
            startTime = endTime;
        }

        public static void Main(string[] args)
        {
            var arguments = new Docopt().Apply(Usage, args, version: Version, exit: true);

            string fixedWord = arguments["<word>"].ToString().ToLower();
            string corpus = arguments["<corpus>"].ToString();
            int range = arguments["-r"].AsInt;
            int limit = arguments["-l"].AsInt;
            double frequencyWeight = double.Parse(arguments["-f"].ToString());
            double similarityWeight = double.Parse(arguments["-s"].ToString());

            int wordSize = fixedWord.Length;
            int minWordLength = Math.Max(1, wordSize - range);
            int maxWordLength = wordSize + range;

            Console.WriteLine($"Unfixing \"{fixedWord}\". Searching {corpus} for words " +
                $"{minWordLength} to {maxWordLength} characters long.");


            DateTimeOffset time = DateTimeOffset.Now;
            (int maxFrequnecy, var wordFrequencies) =
                ParseWordLsit(corpus, minWordLength, maxWordLength);
            LogTask(ref time, "parsing the word list");

            var distanceMeasure = new WeightedLevenshtein(new QwertyCharSub());
            double minDistance = 0.0;
            double maxDistance = 0.0;
            var distanceScore = new Dictionary<string, double>();

            var frequencyScore = new Dictionary<string, double>();

            // Calculate likelihoods and similarities
            foreach (string word in wordFrequencies.Keys) {
                frequencyScore[word] = (double) wordFrequencies[word] / maxFrequnecy;
                double similarity = distanceMeasure.Distance(fixedWord, word);
                minDistance = Math.Min(minDistance, similarity);
                maxDistance = Math.Max(maxDistance, similarity);
                distanceScore[word] = similarity;
            }
            LogTask(ref time, "calculating distance and frequency scores");

            // Convert the distance measure (in which "0" is most-similar) to a similarity score
            // in the range [0, 1], where "1" is most-similar.
            var likelihoodScore = new Dictionary<string, double>();
            foreach (string word in distanceScore.Keys) {
                double normalizedDistance = distanceScore[word] / maxDistance;
                likelihoodScore[word] = (1 - normalizedDistance) * similarityWeight +
                    frequencyScore[word] * frequencyWeight;
            }
            LogTask(ref time, "calculating likelihood scores");

            // Sort the likelihoods
            var orderedLikelihoods = likelihoodScore.OrderByDescending(x => x.Value)
                .ToDictionary(x => x.Key, x => x.Value);
            LogTask(ref time, "sorting likelihood scores");

            Console.WriteLine(string.Join("\n", orderedLikelihoods.Take(limit)
                .Select(p => $"{p.Key}, {p.Value}")));

        }
    }
}
