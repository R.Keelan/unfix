using F23.StringSimilarity;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Unfix {
    public class QwertyCharSub : ICharacterSubstitution {

        public const double MaxDistance = 9.219544457;
        private readonly Dictionary<char, Point> _characterCoordinates;

        public QwertyCharSub()
        {
            _characterCoordinates = new Dictionary<char, Point> {
                ['q'] = new Point(0, 0),
                ['w'] = new Point(1, 0),
                ['e'] = new Point(2, 0),
                ['r'] = new Point(3, 0),
                ['t'] = new Point(4, 0),
                ['y'] = new Point(5, 0),
                ['u'] = new Point(6, 0),
                ['i'] = new Point(7, 0),
                ['o'] = new Point(8, 0),
                ['p'] = new Point(9, 0),
                ['a'] = new Point(0, 1),
                ['s'] = new Point(1, 1),
                ['d'] = new Point(2, 1),
                ['f'] = new Point(3, 1),
                ['g'] = new Point(4, 1),
                ['h'] = new Point(5, 1),
                ['j'] = new Point(6, 1),
                ['k'] = new Point(7, 1),
                ['l'] = new Point(8, 1),
                ['z'] = new Point(0, 2),
                ['x'] = new Point(1, 2),
                ['c'] = new Point(2, 2),
                ['v'] = new Point(3, 2),
                ['b'] = new Point(4, 2),
                ['n'] = new Point(5, 2),
                ['m'] = new Point(6, 2)
            };

        }

        public double Cost(char c1, char c2)
        {
            Point p1 = _characterCoordinates[c1];
            Point p2 = _characterCoordinates[c2];

            double dist = Math.Sqrt(Math.Pow((p1.X - p2.X), 2) + Math.Pow((p1.Y - p2.Y), 2));
            return dist / MaxDistance;
        }
    }
}
