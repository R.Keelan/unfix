using NUnit.Framework;
using System;
using Unfix;

namespace UnfixTests
{
    [TestFixture]
    public class QwertyCharSubTests
    {
        private const double Delta = 0.0000001;
        private QwertyCharSub _charSub;
        [OneTimeSetUp]
        public void OneTimeSetup() => _charSub = new QwertyCharSub();

        [TestCase('q', 'q', 0)]
        [TestCase('q', 'w', 0.108465228909328)]
        [TestCase('q', 'w', 0.108465228909328)]
        [TestCase('q', 'e', 0.216930457818656)]
        [TestCase('q', 'r', 0.325395686727984)]
        [TestCase('q', 't', 0.433860915637312)]
        [TestCase('q', 'y', 0.54232614454664)]
        [TestCase('q', 'u', 0.650791373455968)]
        [TestCase('q', 'i', 0.759256602365297)]
        [TestCase('q', 'o', 0.867721831274625)]
        [TestCase('q', 'p', 0.976187060183953)]
        [TestCase('q', 'a', 0.108465228909328)]
        [TestCase('q', 's', 0.153392997769474)]
        [TestCase('q', 'd', 0.242535625036333)]
        [TestCase('q', 'f', 0.342997170285018)]
        [TestCase('q', 'g', 0.447213595499958)]
        [TestCase('q', 'h', 0.553066318754972)]
        [TestCase('q', 'j', 0.65976823024988)]
        [TestCase('q', 'k', 0.766964988847371)]
        [TestCase('q', 'l', 0.874474632195206)]
        [TestCase('q', 'x', 0.242535625036333)]
        [TestCase('q', 'c', 0.306785995538948)]
        [TestCase('q', 'v', 0.391076944437521)]
        [TestCase('q', 'b', 0.485071250072666)]
        [TestCase('q', 'n', 0.584103133520302)]
        [TestCase('q', 'm', 0.685994340570035)]
        [TestCase('z', 'p', 1)]
        public void CostTest(char c1, char c2, double expectedResult) =>
            Assert.AreEqual(expectedResult, _charSub.Cost(c1, c2), Delta);
    }
}
